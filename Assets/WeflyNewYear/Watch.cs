﻿using UnityEngine;
using System.Collections;
using Wshrzzz.UnityUtil;

public class Watch : MonoBehaviour
{

    public UILabel watchTimeLabel;


    private TweenBasic timeTweener;
    private TweenBasic watchFallTweener;
    private Transform controlObject;

    //Create Class Instance
    void Awake()
    {
        s_Instance = this;
    }
    // Use this for initialization
    void Start()
    {
        controlObject = transform.parent;
        Manager.Instance.WatchCountDownEnd += StartWatch;

    }
    void StartWatch()
    {
        StartCoroutine(ShowWatch(MainManager.Instance.watchTime));
    }

    //Watch fall down
    public IEnumerator ShowWatch(float leftTime)
    {
        float watchAnchorVar;

        if (watchFallTweener == null)
        {
            watchFallTweener = TweenBasic.InitTweener(gameObject);
        }
        watchFallTweener.StartTween(TweenBasic.TweenType.EasyOut, 1f);
        do
        {
            watchAnchorVar = watchFallTweener.SimpleLerp(0.23f, -0.23f);
            yield return new WaitForEndOfFrame();
            controlObject.GetComponent<UIAnchor>().relativeOffset.y = watchAnchorVar;
        } while (watchFallTweener.IsTweening);
        yield return StartCoroutine(CountDown(leftTime));
    }

    public void HideWatch()
    {
        controlObject.GetComponent<UIAnchor>().relativeOffset.y = 0.23f;
        //this.transform.parent.gameObject.SetActive(false);
        
    }

    //Watch Prepare Show
    IEnumerator CountDown(float countDownTime)
    {

        int currentSecond;
        if (timeTweener == null)
        {
            timeTweener = TweenBasic.InitTweener(gameObject);
        }
        timeTweener.StartTween(TweenBasic.TweenType.Linear, countDownTime + 1);
        do
        {
            currentSecond = (int)timeTweener.SimpleLerp(countDownTime + 1, 0f);
            yield return new WaitForSeconds(1f);
            ShowWatchTime(currentSecond);
        } while (timeTweener.IsTweening);
        HideWatch();
        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(CaptureCamera.Instance.WatchPhoto());
        MainManager.Instance.ChangePanel(2);
    }

    //Show Watch time
    void ShowWatchTime(int timeText)
    {
        watchTimeLabel.text = string.Format("{0}", timeText);
    }


    // Update is called once per frame
    void Update()
    {

    }
    private static Watch s_Instance;
    public static Watch Instance
    {
        get
        {
            if (s_Instance != null)
            {
                return s_Instance;
            }
            else
            {
                GUILogDisplay.LogError("There isn't a Watch in the scene.");
                return null;
            }
        }
    }

}
