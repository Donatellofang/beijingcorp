﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Wshrzzz.UnityUtil;


public class PrepareGallery : MonoBehaviour
{
    public Dictionary<string ,Texture2D> allPicTex;

 
    public List<Texture2D> testImagesWidget;
    public Dictionary<string, int> atlasNameToCount;  


    void Awake()
    {
        s_Instance = this;

        testImagesWidget = new List<Texture2D>();
        allPicTex = new Dictionary<string, Texture2D>();
        atlasNameToCount = new Dictionary<string, int>();
    }
    // Use this for initialization
    void Start()
    {
               
    }

    public void PreLoadResouces()
    {
        ResetStorage();        
        int i = 0;
        int last_i = 0;
        string indexSign;
        
        //Load Pictures Resources
        string imagePath = (UnityEngine.Application.streamingAssetsPath + "/Textures/");
        string[] allImages = System.IO.Directory.GetFiles(imagePath, "*.png");
        string atlasName = string.Format("Background Texture");
        i = 0;
        do
        {
            indexSign = string.Format("Background Texture{0}", i);
            byte[] bytes = System.IO.File.ReadAllBytes(allImages[i]);
            allPicTex.Add(indexSign, new Texture2D(10, 10, TextureFormat.RGB24, false));
            allPicTex[indexSign].LoadImage(bytes);
            i++;
        } while (i < allImages.Length);
        last_i = i;
        atlasNameToCount.Add(atlasName,last_i);

        //Load Words Resources
        foreach (KeyValuePair<string, Texture2D> kvp in allPicTex)
        {
            testImagesWidget.Add(kvp.Value);
        }

    }

    
    
    public void ResetStorage()
    {
        allPicTex.Clear();
        testImagesWidget.Clear();
    }
    // Update is called once per frame
    void Update()
    {

    }

    private static PrepareGallery s_Instance = null;
    public static PrepareGallery Instance
    {
        get
        {
            if (s_Instance != null)
            {
                return s_Instance;
            }
            else
            {
                GUILogDisplay.Log("There is no PrepareGallery in scene");
                return null;
            }
        }
    }

}
