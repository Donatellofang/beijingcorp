﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Wshrzzz.UnityUtil;

public class MainManager : MonoBehaviour
{

    public List<GameObject> progressPanel;

    public int watchTime=10;

    private int manualNextPanelNum;
    private bool currentForeachPanelState;
    private GameObject activeObject;
    private bool finishOnce = false;


    void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        progressPanel[1].SetActive(false);
        progressPanel[0].SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {

            Application.Quit();
        }
        if (Input.GetKeyDown(KeyCode.W))
        {

            if (!finishOnce)
            {

                PanelButtonEvent();
                finishOnce = true;
            }


        }
        if (Input.GetKeyUp(KeyCode.W))
        {
            finishOnce = false;
        }
    }
    public void PanelButtonEvent()
    {
        foreach (var item in progressPanel)
        {
            if (item.activeInHierarchy)
            {

                currentForeachPanelState = true;
                activeObject = item;
            }


        }


        if (currentForeachPanelState)
        {
            manualNextPanelNum = progressPanel.IndexOf(activeObject);

            switch (manualNextPanelNum)
            {
                case 0:
                    ChangePanel(2);
                    break;
                case 1:
                    ChangePanel(1);
                    break;
                //case 2:
                //    ChangePanel(4);
                //    break;
                //case 3:
                //    ChangePanel(1);
                //    break;
                default:
                    break;

            }
        }
        currentForeachPanelState = false;


    }
    public void ChangePanel(int panelToShow)
    {
        switch (panelToShow)
        {
            case 1:
                progressPanel[1].SetActive(false);
                progressPanel[0].SetActive(true);

                break;
            case 2:
                InputArgument();
                progressPanel[0].SetActive(false);
                progressPanel[1].SetActive(true);

                break;
            case 3:
                //                 progressPanel[1].SetActive(false);
                //                 progressPanel[2].SetActive(true);
                

                break;
            case 4:
                progressPanel[2].SetActive(false);
                progressPanel[3].SetActive(true);

                break;
            default:
                break;
        }


    }
    void InputArgument()
    {
        watchTime = 3;
        Manager.Instance.ProcessWatch();
        
    }
    private static MainManager s_Instance = null;
    public static MainManager Instance
    {
        get
        {
            if (s_Instance != null)
            {
                return s_Instance;
            }
            else
            {
                GUILogDisplay.Log("There is no MainManager in scene");
                return null;
            }
        }
    }
}
