﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Wshrzzz.UnityUtil;


public class UpdateGallery : MonoBehaviour
{

    private int count;

    void Awake()
    {
        s_Instance = this;
    }
    // Use this for initialization
    void Start()
    {
        
    }

    
    public int CalculateNewMember(int currentValue,bool isLeft)
    {
        count = ShowGallery.Instance.currentAtlas.Count;
        if (isLeft)
        {

            if (currentValue == 0)
            {
                return count-1;
            }
            else
            {
                return currentValue - 1;
            }

        } 
        else
        {

            if (currentValue == count-1)
            {
                return 0;
            }
            else
            {
                return currentValue + 1;
            }

        }
        
        
    }
    // Update is called once per frame
    void Update()
    {

    }

    private static UpdateGallery s_Instance = null;
    public static UpdateGallery Instance
    {
        get
        {
            if (s_Instance != null)
            {
                return s_Instance;
            }
            else
            {
                GUILogDisplay.Log("There is no UpdateGallery in scene");
                return null;
            }
        }
    }
}
