﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Wshrzzz.UnityUtil;


public class ShowGallery : MonoBehaviour {

    public List<Texture2D> currentAtlas;


    //Create Instance
    void Awake()
    {
        s_Instance = this;
    }
	// Use this for initialization
	void Start () {
        //transform.gameObject.SetActive(false);
        Show();

	}
    
    public void Show()
    {       
        Prepare();     
    }
    

    
    void Prepare()
    {
        //FetchBtnSign();
        RuntimePrepareGallery();
        PrepareAtlas();
        PreparePic();
        

    }
   
    void RuntimePrepareGallery()//Because pictures are so many.
    {
        PrepareGallery.Instance.allPicTex.Clear();//Clear last Loaded Resources
        PrepareGallery.Instance.PreLoadResouces();
    }
    void PrepareAtlas()
    {
        currentAtlas = new List<Texture2D>();
        currentAtlas.Clear();//Clear Last Gallery;
        string index = string.Format("Background Texture");
        int picsCount = PrepareGallery.Instance.atlasNameToCount[index];
        bool LessThanThree=(picsCount<3);
        if (LessThanThree)
        {
            SingleOrTwo(picsCount);
        } 
        else
        {
            for (int i = 0; i < picsCount; i++)
            {
                string nameIndex = string.Format("{0}{1}",index, i);

                currentAtlas.Add(PrepareGallery.Instance.allPicTex[nameIndex]);
            }
        }

    }
    void SingleOrTwo(int picsCount)
    {
        if (picsCount==1)
        {
            string nameIndex;
            for (int i = 0; i < picsCount+2; i++)
            {
                nameIndex = string.Format("Background Texture0");
                currentAtlas.Add(PrepareGallery.Instance.allPicTex[nameIndex]);
            }

        } 
        else 
        {
            string nameIndex;
            for (int i = 0; i < picsCount; i++)
            {
                nameIndex = string.Format("Background Texture{0}", i);
                currentAtlas.Add(PrepareGallery.Instance.allPicTex[nameIndex]);              
            }
            nameIndex = string.Format("Background Texture0");
            currentAtlas.Add(PrepareGallery.Instance.allPicTex[nameIndex]);
        }
        
    }
    void PreparePic()
    {
        int i = 0;
        foreach (var item in BoardsManager.Instance.boards)//At Least Three Pictures
        {
            item.GetComponent<UITexture>().mainTexture = currentAtlas[i];//At Beginning ,Initialize the Boards Textures
            i++;
        }
       
    }
    
   
	// Update is called once per frame
	void Update () {
	
	}
    
    private static ShowGallery s_Instance = null;
    public static ShowGallery Instance
    {
        get
        {
            if (s_Instance != null)
            {
                return s_Instance;
            }
            else
            {
                GUILogDisplay.Log("There is no ShowGallery in scene");
                return null;
            }
        }
    }

}
