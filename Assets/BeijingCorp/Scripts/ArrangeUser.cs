﻿using UnityEngine;
using System.Collections;
using Windows.Kinect;
using Wshrzzz.UnityUtil;

using System.Runtime.InteropServices;

public class ArrangeUser : MonoBehaviour
{
   
    public Texture2D userTex;
    public GameObject showBoard;

    [HideInInspector]
    public int dimensionAdd;
    [HideInInspector]
    public float offsetXAdd;
    [HideInInspector]
    public float offsetYAdd;
    [HideInInspector]
    public float relativeOffset_x;
    [HideInInspector]
    public float relativeOffset_y;


    private int x;
    private int y;
    private UIAnchor anchor;
    private bool finishArrange=false;
    private KinectManager manager;

    private KinectInterop.SensorData sensorData;

    //Create Class Instance
    void Awake()
    {
        s_Instance = this;
    }
    // Use this for initialization
    void Start()
    {
        anchor = GetComponent<UIAnchor>();
        dimensionAdd = 512;
        offsetXAdd = 0;
        offsetYAdd = 0;

        relativeOffset_x = 0;
        relativeOffset_y = 0;

    }
    void ArrangePhoto()
    {
        ManualArrange.Instance.Arrange += ArrangeDimension;
        /*ManualArrange.Instance.Arrange += ArrangePosition;*/
    }

    void Initialization()
    {
        if (!finishArrange)
        {
            ArrangeDimension();
            ArrangeOffsetX();
            ArrangeOffsetY();
            finishArrange = true;
        }
    }

    void RuntimeShowUser()
    {
        userTex = manager.GetUsersLblTex();
        showBoard.GetComponent<UITexture>().mainTexture = userTex;

    }

    void ArrangeDimension()
    {
        x = manager.GetUsersLblTex().width;
        y = manager.GetUsersLblTex().height;
        int currentDimension_w=0;
        
        if (PlayerPrefs.GetInt("dimensionAdd")!=0)
        {
            currentDimension_w = x + PlayerPrefs.GetInt("dimensionAdd");
        }
        else
        {
           currentDimension_w = x + dimensionAdd;
        }
        showBoard.GetComponent<UIWidget>().SetDimensions(currentDimension_w, currentDimension_w*y/x);
    }
    void ArrangeOffsetX()
    {
        if (PlayerPrefs.GetFloat("offsetXAdd")!=0)
        {
            relativeOffset_x = PlayerPrefs.GetFloat("offsetXAdd");
        } 
        else
        {
            relativeOffset_x = offsetXAdd;
        }
        anchor.relativeOffset.x = relativeOffset_x;
    }

    void ArrangeOffsetY()
    {

        if (PlayerPrefs.GetFloat("offsetYAdd") != 0)
        {
            relativeOffset_y = PlayerPrefs.GetFloat("offsetYAdd");
        }
        else
        {
            relativeOffset_y = offsetYAdd;
        }
        anchor.relativeOffset.y = relativeOffset_y;

    }



    public void ArrangeX(bool isLeft)
    {
        if (isLeft)
        {
            offsetXAdd = PlayerPrefs.GetFloat("offsetXAdd") - 0.01f;
        } 
        else
        {
            offsetXAdd = PlayerPrefs.GetFloat("offsetXAdd") + 0.01f;
        }
        PlayerPrefs.SetFloat("offsetXAdd", offsetXAdd);
        finishArrange = false;
    }
    public void ArrangeY(bool isUpgrade)
    {
        if (isUpgrade)
        {
            offsetYAdd = PlayerPrefs.GetFloat("offsetYAdd") + 0.01f;            
        }
        else
        {
            offsetYAdd = PlayerPrefs.GetFloat("offsetYAdd") - 0.01f;
        }
        PlayerPrefs.SetFloat("offsetYAdd", offsetYAdd);
        finishArrange = false;
    }

    public void DimensionChange(bool isBigger)
    {
        if (isBigger)
        {
            dimensionAdd = PlayerPrefs.GetInt("dimensionAdd") + 10;         
        } 
        else
        {
            dimensionAdd = PlayerPrefs.GetInt("dimensionAdd") - 10;            
        }
        PlayerPrefs.SetInt("dimensionAdd", dimensionAdd);
        finishArrange = false;

    }
    
    // Update is called once per frame
    void Update()
    {
        manager = KinectManager.Instance;
        if (manager&&manager.IsInitialized()&&manager.IsUserDetected())
        {
            RuntimeShowUser();
            Initialization();
        }

    }

    private static ArrangeUser s_Instance = null;
    public static ArrangeUser Instance
    {
        get
        {
            if (s_Instance != null)
            {
                return s_Instance;
            }
            else
            {
                GUILogDisplay.LogError("There is no ArrangeUser in the Scenes");
                return null;
            }
        }

    }

}
