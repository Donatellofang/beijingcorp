﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Wshrzzz.UnityUtil;

public class SlidePic : MonoBehaviour
{

    public float xVelocity = 20f;
    public float smoothTime = 10f;
    
    private float limit_Y;
    private float object_x;
    private TweenBasic moveTweener;
    private bool startMove = false;
    private Vector2 pressedPoint;
    private Vector2 releasedPoint;
    private float pressedTime;
    private float releasedTime;
    private bool autoMove = false;


    //Create Instance
    void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        limit_Y = transform.localPosition.y;
    }

    private void OnEnable()//at their Parent Script add the Handlers
    {
//         GetComponent<PressGesture>().Pressed += pressedHandler;
//         GetComponent<ReleaseGesture>().Released += releasedHandler;
    }

    private void OnDisable()//at their Parent Script subtract the Handlers
    {
// 
//         GetComponent<PressGesture>().Pressed -= pressedHandler;
//         GetComponent<ReleaseGesture>().Released -= releasedHandler;
    }

    private void pressedHandler(object sender, EventArgs e)
    {

        autoMove = false;
        CirculateBoards.Instance.startCirculate = true;

        pressedTime = Time.time;
        //MoveTouch.Instance.firstPointTime = Time.time;
    }

    private void releasedHandler(object sender, EventArgs e)
    {
        autoMove = true;
        startMove = true;

        releasedTime = Time.time;
        //MoveTouch.Instance.secondPointTime = Time.time;
        //MoveTouch.Instance.AnalysisQuickTouch();

    }


    IEnumerator AnalysisCurrentPos()
    {
        object_x = transform.position.x;

        if (object_x * 100 < PoleInfo.Instance.centerLeftPole_x * 100)//Left Direction
        {
            //Debug.Log("//Left Direction");
            yield return StartCoroutine(MoveLeft());

        }
        else if (object_x * 100 > PoleInfo.Instance.centerRightPole_x * 100)//Right Direction
        {
            //Debug.Log("//Right Direction");
            yield return StartCoroutine(MoveRight());            

        }
        else if (!Mathf.Approximately(object_x * 100, PoleInfo.Instance.centerPole_x * 100))//Back Center Pole position
        {
            yield return StartCoroutine(Move(PoleInfo.Instance.centerPole_x));

        }


    }
    public IEnumerator MoveLeft()
    {
        yield return StartCoroutine(Move(PoleInfo.Instance.leftPole_x));
        CirculateBoards.Instance.isLeft = true;
    }
    public IEnumerator MoveRight()
    {
        yield return StartCoroutine(Move(PoleInfo.Instance.rightPole_x));
        CirculateBoards.Instance.isRight = true;
    }

    IEnumerator Move(float pole_x)
    {

        if (moveTweener == null)
        {
            moveTweener = TweenBasic.InitTweener(gameObject);
        }

        moveTweener.StartTween(TweenBasic.TweenType.Linear, smoothTime, reset: false);
        do
        {
            transform.position = new Vector3(moveTweener.SimpleLerp(transform.position.x, pole_x), transform.position.y, 0);
            yield return new WaitForEndOfFrame();
        } while (moveTweener.IsTweening);

    }


    // Update is called once per frame
    void Update()
    {
        //transform.localPosition = new Vector3(transform.localPosition.x, Mathf.Lerp(transform.localPosition.y, limit_Y, 1), 0);
        float diffrenceTime = releasedTime-pressedTime;
        //if (autoMove && startMove && diffrenceTime > MoveTouch.Instance.quickTouchPeriod)//diffrenceTime > 0.5f is After analysis of quick Touch,autoMove is After releasing board,startMove Allow Enter this if sentence once
        if (autoMove && startMove) 
        {
            startMove = false;
            StartCoroutine(AnalysisCurrentPos());

        }
    }

    private static SlidePic s_Instance = null;
    public static SlidePic Instance
    {
        get
        {
            if (s_Instance != null)
            {
                return s_Instance;
            }
            else
            {
                GUILogDisplay.Log("There is no SlidePic in scene");
                return null;
            }
        }
    }


}
