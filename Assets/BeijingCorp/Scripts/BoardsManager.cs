﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Wshrzzz.UnityUtil;

public class BoardsManager : MonoBehaviour {

    public GameObject boardsContainerPrefab;
    public GameObject containerParent;
    public GameObject oldBoardsContainer;

    public List<GameObject> boards;
    public List<Vector3> originalBoardsPos;

  
    private GameObject m_OldBoardsContainer;
    private GameObject m_newBoardsContainer;
    private Vector3 boardsContainerPos;

    //Create Instance
    void Awake()
    {
        s_Instance = this;
    }

	// Use this for initialization
	void Start () {

        GetBoardsInfo();
	}
    void GetBoardsInfo()
    {
        boardsContainerPos = oldBoardsContainer.transform.localPosition;

        originalBoardsPos = new List<Vector3>();
        foreach (var item in boards)
        {
            originalBoardsPos.Add(item.transform.localPosition);
        }
    }

    public void CreateContainer()
    {     
        m_OldBoardsContainer = GameObject.Find("Background");

        m_newBoardsContainer = Instantiate(boardsContainerPrefab) as GameObject;
        m_newBoardsContainer.transform.parent = containerParent.transform;
        m_newBoardsContainer.transform.localScale = Vector3.one;
        m_newBoardsContainer.transform.localPosition = boardsContainerPos;

        int i = 0;
        foreach (var item in boards)
        {
            item.transform.parent = m_newBoardsContainer.transform;
            item.transform.localPosition = originalBoardsPos[i];//Arrange current All Boards Position To their Original Position.Even though Position has Changed Before
            i++;
        }
        Destroy(m_OldBoardsContainer);
        m_newBoardsContainer.name = "Background";
        
    }
  
	// Update is called once per frame
	void Update () {
	
	}

    private static BoardsManager s_Instance = null;
    public static BoardsManager Instance
    {
        get
        {
            if (s_Instance != null)
            {
                return s_Instance;
            }
            else
            {
                GUILogDisplay.Log("There is no BoardsManager in scene");
                return null;
            }
        }
    }

}
