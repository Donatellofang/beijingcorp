﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Wshrzzz.UnityUtil;


public class PoleInfo : MonoBehaviour {

    public List<Transform> poles;

    public float leftPole_x;
    public float centerLeftPole_x;
    public float centerPole_x;
    public float centerRightPole_x;
    public float rightPole_x;

    void Awake()
    {
        s_Instance = this;
    }

	// Use this for initialization
	void Start () {
        
        leftPole_x = poles[0].position.x;
        centerLeftPole_x = poles[1].position.x;
        centerPole_x = poles[2].position.x;
        centerRightPole_x = poles[3].position.x;
        rightPole_x = poles[4].position.x;

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private static PoleInfo s_Instance = null;
    public static PoleInfo Instance
    {
        get
        {
            if (s_Instance != null)
            {
                return s_Instance;
            }
            else
            {
                GUILogDisplay.LogError("There is no PoleInfo Instance in the scene");

                return null;
            }
        }

    }
}
