﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Wshrzzz.UnityUtil;


public class CaptureCamera : MonoBehaviour {
    public Texture2D screenshoot;
    public byte[] pngData;


    void Awake()
    {
        s_Instance = this;
        
    }
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
         
	}



    public IEnumerator WatchPhoto()
    {
        yield return StartCoroutine(OnTakePhoto());

        ShowCapture(screenshoot);
    }

    //Screen shoot
    IEnumerator OnTakePhoto()
    {

        yield return new WaitForEndOfFrame();
		//int imgW = Mathf.FloorToInt (Screen.width * 0.75f);
        int imgW = Mathf.FloorToInt(Screen.width);

		screenshoot = new Texture2D(imgW,Screen.height);
		screenshoot.ReadPixels(new Rect(Screen.width - imgW, 0f, imgW, Screen.height), 0, 0);
        screenshoot.Apply();
        pngData = screenshoot.EncodeToPNG();
        
        
    }

    void ShowCapture(Texture2D photo)
    {
        renderer.material.mainTexture = photo;
        renderer.enabled = true;       
    }

    

    #region Instance
    private static CaptureCamera s_Instance;
    public static CaptureCamera Instance
    {
        get
        {
            if (s_Instance != null)
            {
                return s_Instance;
            }
            else
            {
                GUILogDisplay.LogError("There isn't a CaptureCamera in the scene.");
                return null;
            }
        }
    }
    #endregion
}
