﻿using UnityEngine;
using System.Collections;
using ZXing;
using ZXing.QrCode;

public class CreateQR : MonoBehaviour
{

    public static Color32[] Encode(string textForEncoding, int width, int height)
    {
        BarcodeWriter writer = new BarcodeWriter();
        writer.Format = BarcodeFormat.QR_CODE;
        writer.Options = new QrCodeEncodingOptions();
        writer.Options.Height = height;
        writer.Options.Width = width;
        writer.Options.Margin = 0;
        return writer.Write(textForEncoding);
    }
}
