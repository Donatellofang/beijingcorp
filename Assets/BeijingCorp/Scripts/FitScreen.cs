﻿using UnityEngine;
using System.Collections;

//Quad 自动适配屏幕大小
public class FitScreen : MonoBehaviour {

    //对应相机
    public Camera cam;
    //距相机位置距离
    public float distance;
    //分辨率
    public Vector2 aspect;
    //是否使用屏幕分辨率，开启则忽视上一项分辨率设定
    public bool useScreenAspect;

	// Use this for initialization
	void Start () {
        float height = 0f;
        float width = 0f;
        if (useScreenAspect)
        {
            //根据相机模式计算高度（正交，投影）
            if (cam.orthographic)
            {
                height = cam.orthographicSize * 2.01f;
            }
            else
            {
                height = Mathf.Tan(cam.fieldOfView / 360f * Mathf.PI) * distance * 1.01f * 2f;
            }

            width = (float)Screen.width / (float)Screen.height * height;
            transform.localPosition = new Vector3(0f, 0f, distance);
            //使Quad暂时脱离父物体，修正因父物体累计的缩放因子造成的误差
            Transform parent = transform.parent;
            transform.parent = null;
            transform.localScale = new Vector3(width, height, 1f);
            transform.parent = parent;
        } 
        else
        {
            //根据视频长宽比与屏幕长宽比的对比选择不同的计算方式
            if (((float)Screen.width / (float)Screen.height) > (aspect.x / aspect.y))
            {
                if (cam.orthographic)
                {
                    width = cam.orthographicSize * 2.01f * (float)Screen.width / (float)Screen.height;
                } 
                else
                {
                    width = Mathf.Tan(cam.fieldOfView / 360f * Mathf.PI) * distance * 1.01f * 2f * (float)Screen.width / (float)Screen.height;
                }

                height = width * aspect.y / aspect.x;
                transform.localPosition = new Vector3(0f, 0f, distance);
                Transform parent = transform.parent;
                transform.parent = null;
                transform.localScale = new Vector3(width, height, 1f);
                transform.parent = parent;
            }
            else
            {
                if (cam.orthographic)
                {
                    height = cam.orthographicSize * 2.01f;
                }
                else
                {
                    height = Mathf.Tan(cam.fieldOfView / 360f * Mathf.PI) * distance * 1.01f * 2f;
                }

                width = aspect.x / aspect.y * height;
                transform.localPosition = new Vector3(0f, 0f, distance);
                Transform parent = transform.parent;
                transform.parent = null;
                transform.localScale = new Vector3(width, height, 1f);
                transform.parent = parent;
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
	    
	}
}
