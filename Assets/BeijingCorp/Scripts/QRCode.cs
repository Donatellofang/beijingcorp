﻿using UnityEngine;
using System.Collections;

//第四幕二维码显示
public class QRCode : MonoBehaviour
{

    public UITexture tex;
    private Texture2D m_QR;

    void OnEnable()
    {
        if (UploadPic.Instance != null)
        {
            if (!string.IsNullOrEmpty(UploadPic.Instance.lastGifUrl))
            {
                if (m_QR == null)
                {
                    m_QR = new Texture2D(256, 256);
                }

                m_QR.SetPixels32(CreateQR.Encode(UploadPic.Instance.lastGifUrl, 256, 256));
                m_QR.Apply();
                tex.mainTexture = m_QR;
            }
        }
    }
}
