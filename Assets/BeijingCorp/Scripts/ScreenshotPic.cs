﻿using UnityEngine;
using System.Collections;

public class ScreenshotPic : MonoBehaviour {

    public UITexture tex;
    public Vector2 aspect;

    void OnEnable()
    {

        if (CaptureCamera.Instance!=null)
        {
            if (CaptureCamera.Instance.screenshoot!=null)
            {
                tex.mainTexture = CaptureCamera.Instance.screenshoot;
                
                if ((aspect.x / aspect.y) > ((float)CaptureCamera.Instance.screenshoot.width / (float)CaptureCamera.Instance.screenshoot.height))
                {
                    tex.width = (int)(aspect.y * (float)CaptureCamera.Instance.screenshoot.width/(float)CaptureCamera.Instance.screenshoot.height);
                } 
                else
                {
                    tex.height = (int)(aspect.x * (float)CaptureCamera.Instance.screenshoot.height / (float)CaptureCamera.Instance.screenshoot.width);

                }
                
            }
            
        }
    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
