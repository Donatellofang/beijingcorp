﻿using UnityEngine;
using System.Collections;
using Wshrzzz.UnityUtil;

public class GestureBehavious : MonoBehaviour {

    public int userIndex;
    [HideInInspector]
    public bool finishGestureOnce=true;

    private GestureListener gestureListener;
    private KinectManager m_KinectManagerInstance;
    private bool activeOnce;

    //Create Class Instance
    void Awake()
    {
        s_Instance = this;
    }
	// Use this for initialization
	void Start () {
        //get the gesture Listener
        gestureListener = Camera.main.GetComponent<GestureListener>();
        m_KinectManagerInstance = KinectManager.Instance;
        activeOnce = true;
	}
	
	// Update is called once per frame
	void Update () {
        // dont run Update() if there is no user
        if (!m_KinectManagerInstance || !m_KinectManagerInstance.IsInitialized() || !m_KinectManagerInstance.IsUserDetected())
            return;
        if (gestureListener)
        {
            if (gestureListener.IsSwipeLeft())
            {
                if (finishGestureOnce)
                {
                    finishGestureOnce = false;
                    SlideToNext();
                }

            }
            if (gestureListener.IsSwipeRight())
            {
                if (finishGestureOnce)
                {
                    finishGestureOnce = false;
                    SlideToPrevious();
                }

            }
            HoldUpHand();
        }
        
      
	}
    void HoldUpHand()
    {
        //Have detected User
        if (m_KinectManagerInstance.IsUserCalibrated(m_KinectManagerInstance.GetUserIdByIndex(userIndex)))
        {
            if ( CalculateDifference(userIndex)>0&&activeOnce)
            {
                activeOnce = false;
                Manager.Instance.ProcessWatch();
                
            }
        }
    }
    float CalculateDifference(int index)
    {
        long _userId = m_KinectManagerInstance.GetUserIdByIndex(index);
        float difference = m_KinectManagerInstance.GetJointPosition(_userId, (int)Windows.Kinect.JointType.HandRight).y - m_KinectManagerInstance.GetJointPosition(_userId, (int)Windows.Kinect.JointType.SpineShoulder).y;
        return difference;
    }





    void SlideToPrevious()
    {
        StartCoroutine(SlidePic.Instance.MoveRight());
        CirculateBoards.Instance.startCirculate = true;


    }
    void SlideToNext()
    {
        StartCoroutine(SlidePic.Instance.MoveLeft());
        CirculateBoards.Instance.startCirculate = true;

    }
    private static GestureBehavious s_Instance = null;
    public static GestureBehavious Instance
    {
        get
        {
            if (s_Instance != null)
            {
                return s_Instance;
            }
            else
            {
                GUILogDisplay.LogError("There is no GestureBehavious in the Scenes");
                return null;
            }
        }

    }

}
