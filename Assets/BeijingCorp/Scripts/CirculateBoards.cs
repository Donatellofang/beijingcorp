﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Wshrzzz.UnityUtil;


public class CirculateBoards : MonoBehaviour {

    [HideInInspector]
    public bool startCirculate=false;
    [HideInInspector]
    public bool isRight=false;
    [HideInInspector]
    public bool isLeft = false;

    public GameObject boardPrefab;
    public List<string> boardsName;
    public List<GameObject> boards;


    private GameObject m_LeftCircleBoard;
    private GameObject m_RightCircleBoard;

    private GameObject boardsParent;
    private bool steady = true;
    private float rightDifference;
    private float leftDifference;
    private Texture2D m_NewCircleBoardPic;
    
    public delegate void Del(int a,int b);

    void Awake()
    {
        s_Instance = this;
      
    }

	// Use this for initialization
    void Start()
    {
        rightDifference = Mathf.Abs(transform.position.x - PoleInfo.Instance.rightPole_x);
        leftDifference = Mathf.Abs(transform.position.x - PoleInfo.Instance.leftPole_x);
    }

    void AffectGallery(bool isMoveLeft,out Texture2D newCircleBoardPic)
    {
        if (isMoveLeft)
        {
            
            Texture2D x = BoardsManager.Instance.boards[0].GetComponent<UITexture>().mainTexture as Texture2D;
            int currentIndex = ShowGallery.Instance.currentAtlas.IndexOf(x); //Do not Have same Pictures  in file
            int nextIndex=UpdateGallery.Instance.CalculateNewMember(currentIndex,true);

           
            newCircleBoardPic = ShowGallery.Instance.currentAtlas[nextIndex];
        } 
        else
        {
            
            Texture2D x = BoardsManager.Instance.boards[BoardsManager.Instance.boards.Count - 1].GetComponent<UITexture>().mainTexture as Texture2D;
            int currentIndex = ShowGallery.Instance.currentAtlas.IndexOf(x); //Do not Have same Pictures  in file
            //Debug.Log("currentIndex: " + currentIndex);
            int nextIndex = UpdateGallery.Instance.CalculateNewMember(currentIndex, false);
            //Debug.Log("nextIndex: " + nextIndex);
            newCircleBoardPic = ShowGallery.Instance.currentAtlas[nextIndex];

        }

    }


    public void Circulate(bool isRightDir)
    {
        //Debug.Log("Circulate");
        boardsParent = GameObject.Find("Background");

        if (isRightDir)//Slide RightDirection
        {
          
            m_LeftCircleBoard = Instantiate(boardPrefab) as GameObject;//Create New Board
            
            m_LeftCircleBoard.transform.parent = boardsParent.transform;
            m_LeftCircleBoard.transform.localScale = Vector3.one;
            m_LeftCircleBoard.transform.localPosition = BoardsManager.Instance.originalBoardsPos[0];

            AffectGallery(true,out m_NewCircleBoardPic);
            m_LeftCircleBoard.GetComponent<UITexture>().mainTexture = m_NewCircleBoardPic;

            GameObject waitDestroyObject = BoardsManager.Instance.boards[BoardsManager.Instance.boards.Count - 1];

            BoardsManager.Instance.boards.RemoveAt(BoardsManager.Instance.boards.Count-1);//Deal Boards List Changes
            BoardsManager.Instance.boards.Insert(0,m_LeftCircleBoard);
            Destroy(waitDestroyObject);//Delete Bypass board


        } 
        else//Slide LeftDirection
        {
            m_RightCircleBoard = Instantiate(boardPrefab) as GameObject;

            m_RightCircleBoard.transform.parent = boardsParent.transform;
            m_RightCircleBoard.transform.localScale = Vector3.one;
            m_RightCircleBoard.transform.localPosition = BoardsManager.Instance.originalBoardsPos[BoardsManager.Instance.originalBoardsPos.Count-1];

            //AffectGallery(true);
            AffectGallery(false, out m_NewCircleBoardPic);
            m_RightCircleBoard.GetComponent<UITexture>().mainTexture = m_NewCircleBoardPic;
            GameObject waitDestroyObject = BoardsManager.Instance.boards[0];

            BoardsManager.Instance.boards.RemoveAt(0);
            BoardsManager.Instance.boards.Insert(BoardsManager.Instance.boards.Count,m_RightCircleBoard);
            Destroy(waitDestroyObject);
            

        }
        RenameBoardsItem();
        steady = true;
        BoardsManager.Instance.CreateContainer();
        FinsihCirculate();
        GetShowingPicNum();
    }
    void FinsihCirculate()
    {
        GestureBehavious.Instance.finishGestureOnce = true;
    }
    void GetShowingPicNum()
    {
        Texture2D x = BoardsManager.Instance.boards[1].GetComponent<UITexture>().mainTexture as Texture2D;
        int currentIndex = ShowGallery.Instance.currentAtlas.IndexOf(x);
        //PageNumber.Instance.SetNumber(currentIndex, ShowGallery.Instance.currentAtlas.Count);
    }
    public void ExchangeBoard()
    {
        if (steady&&startCirculate)
        {
            //After auto Move to steady state           
            
            float compareValue =transform.position.x;
            if (FloatEqualTo(compareValue,PoleInfo.Instance.rightPole_x,1f))//Right Direction
            {
                steady = false;
                Circulate(true);
                startCirculate = false;
                isRight = false;

                
            }
            else if (FloatEqualTo(compareValue , PoleInfo.Instance.leftPole_x, 1f))//Left Direction
            {
                steady = false;
                Circulate(false);
                startCirculate = false;
                isLeft = false;


            }
            

        }
        
        
    }

    void RenameBoardsItem()
    {
        for (int i = 0; i < BoardsManager.Instance.boards.Count; i++)
        {
            BoardsManager.Instance.boards[i].name = boardsName[i];
        }
    }
    public bool FloatEqualTo(float left, float right, float epsilon)
    {
        return Mathf.Abs(left*100 - right*100) <= epsilon;
    }

	// Update is called once per frame
	void Update () {
        
        ExchangeBoard();
	}

    private static CirculateBoards s_Instance = null;
    public static CirculateBoards Instance
    {
        get
        {
            if (s_Instance != null)
            {
                return s_Instance;
            }
            else
            {
                GUILogDisplay.Log("There is no CirculateBoards in scene");
                return null;
            }
        }
    }
}
