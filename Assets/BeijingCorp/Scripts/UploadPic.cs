﻿using UnityEngine;
using System.Collections;
using System.IO;
using MiniJSON;
using System.Collections.Generic;
using Wshrzzz.UnityUtil;

//上传照片
public class UploadPic : MonoBehaviour
{

    public string lastGifUrl;

    // Use this for initialization
    void Start()
    {
        m_Instance = this;
    }

    public IEnumerator Upload(byte[] data)
    {
        
        lastGifUrl = null;

        Debug.Log(data.Length);

        WWWForm form = new WWWForm();
        form.AddField("DefaultPhotoinfo[app_id]", "2014006");
        form.AddField("DefaultPhotoinfo[image]", "");
        form.AddBinaryData("DefaultPhotoinfo[image]", data, "WeFlyNewYear.png", "image/png");

        WWW www = new WWW("http://photo.easyarshow.com/index.php?r=photoUpload/create", form);
        yield return www;

        if (www.error == null)
        {
            
            Dictionary<string, object> jsonData = Json.Deserialize(www.text) as Dictionary<string, object>;
            if (jsonData["errorCode"].ToString() == "0")
            {
                //lastGifUrl = @"http://photo.easyarshow.com/index.php?r=webApi/show&appId=2014005&photoId=" + jsonData["photoId"].ToString();
                //lastGifUrl = @"http://mini.easyarshow.com/chrisAR/index.html?appId=2014005&photoId=" + jsonData["photoId"].ToString();
                lastGifUrl = @"http://mini.easyarshow.com/WfCNY2015/index.html?appId=2014006&photoId=" + jsonData["photoId"].ToString();                       

                Debug.Log(lastGifUrl);
            }
            else
            {
                Debug.LogError("Upload wrong with code: " + jsonData["errorCode"].ToString());
            }

        }
        else
        {
            Debug.LogError(www.error);
        }

        yield return null;
    }

    private static UploadPic m_Instance = null;
    public static UploadPic Instance
    {
        get
        {
            return m_Instance;
        }
    }
}
