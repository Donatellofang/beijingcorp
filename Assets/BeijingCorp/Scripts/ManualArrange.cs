﻿using UnityEngine;
using System.Collections;
using Wshrzzz.UnityUtil;

public class ManualArrange : MonoBehaviour {

    public delegate void ArrangeEventHandler();
    public event ArrangeEventHandler Arrange;

    private bool finishAOnce=false;
    private bool finishBOnce = false;
    private bool finishCOnce = false;
    private bool finishDOnce = false;



    protected virtual void OnArrange()
    {
        if (Arrange!=null)
        {
            Arrange();
        }
        
    }
    public void ArrangeObjective()
    {
        OnArrange();
    }
	//Create Class Instance
    void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
        //Key Plus ,Add User Size
        if (Input.GetKeyDown(KeyCode.KeypadPlus))
        {
            if (!finishAOnce)
            {
                ArrangeUser.Instance.DimensionChange(true);
                finishAOnce = true;
            }
        }
        if (Input.GetKeyUp(KeyCode.KeypadPlus))
        {
            finishAOnce = false;
        }
        //Key Minus,Subtract User Size
        if (Input.GetKeyDown(KeyCode.KeypadMinus))
        {
            if (!finishBOnce)
            {
                ArrangeUser.Instance.DimensionChange(false);                
                finishBOnce = true;
            }
        }
        if (Input.GetKeyUp(KeyCode.KeypadMinus))
        {
            finishBOnce = false;
        }
        // Key LeftArrow,Move user Left
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (!finishCOnce)
            {
                ArrangeUser.Instance.ArrangeX(true);

                finishCOnce = true;
            }
        }
        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            finishCOnce = false;
        }
        // Key RightArrow,Move user Right
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (!finishDOnce)
            {
                ArrangeUser.Instance.ArrangeX(false);
                finishDOnce = true;
            }
        }
        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            finishDOnce = false;
        }
        // Key UpArrow,Move user Up
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (!finishDOnce)
            {
                ArrangeUser.Instance.ArrangeY(true);
                finishDOnce = true;
            }
        }
        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            finishDOnce = false;
        }
        // Key DownArrow,Move user Down
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (!finishDOnce)
            {
                ArrangeUser.Instance.ArrangeY(false);
                finishDOnce = true;
            }
        }
        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            finishDOnce = false;
        } 

    }
    private static ManualArrange s_Instance=null;
    public static ManualArrange Instance
    {
        get
        {
            if (s_Instance!=null)
            {
                return s_Instance;
            } 
            else
            {
                GUILogDisplay.LogError("There is no ManualArrange in the Scenes");
                return null;
            }
        }

    }

}
