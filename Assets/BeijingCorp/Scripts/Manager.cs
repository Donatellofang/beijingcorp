﻿using UnityEngine;
using System.Collections;
using Wshrzzz.UnityUtil;

public class Manager : MonoBehaviour {

    public delegate void ManageEventHandler();
    public event ManageEventHandler MainReset;
    public event ManageEventHandler HandHoldUp;
    public event ManageEventHandler WatchCountDownEnd;
    public event ManageEventHandler ShareQREnd;



    //Create Class Instance
    void Awake()
    {
        s_Instance = this;
    }

    protected virtual void OnHandHoldUp()
    {
        if (HandHoldUp != null)
        {
            HandHoldUp();
            //HandHoldUp.Invoke();
        }

    }
    protected virtual void OnWatchCountDownEnd()
    {
        if (WatchCountDownEnd != null)
        {
            WatchCountDownEnd();
        }

    }
    public void ProcessHandHoldUp()
    {
        OnHandHoldUp();
    }
    public void ProcessWatch()
    {
        OnWatchCountDownEnd();
    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    private static Manager s_Instance = null;
    public static Manager Instance
    {
        get
        {
            if (s_Instance != null)
            {
                return s_Instance;
            }
            else
            {
                GUILogDisplay.LogError("There is no Manager in the Scenes");
                return null;
            }
        }

    }
}
